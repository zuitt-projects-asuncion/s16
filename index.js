/*
	Activity 16

	Create a var called a number that will store the value of the number provided by the user via the prompt.

	Create a for loop that will:
		initialize with the number provided by the user,
		will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

	Create a condition that if the current value is less than or equal to 50, stop the loop.

	Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.

	Create another condition that if teh current value is divisible by 5, print the number.
*/


let number = Number(prompt("Give me a number"));
if (typeof number != "number"){
		alert("Invalid Input")
}//END checking number type		
console.log(number);

for(let i = number; i >= 0; i--){
		
		if(i % 5 === 0){
			console.log(`${i}`);
		} //END if i is divisible by 5

		if(i % 10 == 0){
			console.log(`Skipping ${i}`)
			i--;
		} //END if i is divisible by 10

		if (i <= 50){
			console.log("Limit 50 has been reached. ▓ ▓ Stopping iteration ▓ ▓")
			break;
		} //END break
	


} //END for loop